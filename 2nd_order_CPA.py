import numpy as np
from scipy.stats.stats import pearsonr
import matplotlib.pyplot as plt
import logging as log
# import matlab.engine
import scipy.io as sio
import time
import os

from multiprocessing import Pool, current_process
from functools import partial

import sys
sys.path.append('./../Correlation')
import corr as corr

log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)

nb_doublets = 8
nb_k_hyp = 65536

HW = np.array([bin(byte).count('1') for byte in range(256)]).astype(np.uint8)

Sbox_hex = [0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76, 0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0, 0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15, 0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75, 0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84, 0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF, 0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8, 0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2, 0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73, 0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB, 0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79, 0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08, 0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A, 0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E, 0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF, 0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16]

# Precompute the 16-bit SBox (two 2-bit SBoxes side-by-side)
Sbox_dec_byte = np.array([int(s) for s in Sbox_hex], dtype = np.uint8)
Sbox_dec = np.array([int(s_high)*256+int(s_low) for s_high in Sbox_hex for s_low in Sbox_hex], dtype = np.uint16)

def compute_prediction(plaintexts_path = './plaintexts',
                       predictions_path = './predictions',
                       save_mat = False):
    t0 = time.clock()
    for plaintexts_filename in os.listdir(plaintexts_path):
        plaintexts = np.load(os.path.join(plaintexts_path, plaintexts_filename)).astype(np.uint8)
        break    
    # plaintexts = np.load(os.path.join(plaintexts_path, 'masked_plaintexts.npy')).astype(np.uint8)
    # Merge plaintext bytes into doublets (16 bits)
    plaintexts = 256*plaintexts[:,::2]+plaintexts[:,1::2]
    # Compute the possible key hypotheses
    k_hyps = np.array(range(nb_k_hyp), dtype=np.uint16) #0 to 65535
    # Apply the leakage model
    for doublet in range(nb_doublets):
        log.info("Computing prediction for doublet {0}".format(doublet))
        prediction = Sbox_dec[np.bitwise_xor(plaintexts[:, doublet, np.newaxis], k_hyps)]
        prediction = HW[np.bitwise_xor(prediction/256, prediction%256).astype(np.uint8)]
        np.save(os.path.join(predictions_path, 'prediction_doublet_'+str(doublet)+'.npy'), prediction)
        if save_mat:
            sio.savemat(os.path.join(predictions_path, 'prediction_doublet_'+str(doublet)+'.mat'), {'prediction':prediction.astype(np.float32)})
    log.info("Predictions computed in {0} s.".format(time.clock()-t0))


def compute_prediction_one_doublet(doublet,
                                   plaintexts_path = './plaintexts',
                                   predictions_path = './predictions',
                                   save_mat = False):
    plaintexts_path = './plaintexts'
    predictions_path = './predictions'
    plaintexts = np.load(os.path.join(plaintexts_path, 'masked_plaintexts.npy')).astype(np.uint8)
    # Merge plaintext bytes into doublets (16 bits)
    plaintexts = 256*plaintexts[:,::2]+plaintexts[:,1::2]
    # Compute the possible key hypotheses
    k_hyps = np.array(range(nb_k_hyp), dtype=np.uint16) #0 to 65535
    # Apply the leakage model
    log.info("Computing prediction for doublet {0}".format(doublet))
    prediction = Sbox_dec[np.bitwise_xor(plaintexts[:, doublet, np.newaxis], k_hyps)]
    prediction = HW[np.bitwise_xor(prediction/256, prediction%256).astype(np.uint8)]
    np.save(os.path.join(predictions_path, 'prediction_doublet_'+str(doublet)+'.npy'), prediction)
    if save_mat:
        sio.savemat(os.path.join(predictions_path, 'prediction_doublet_'+str(doublet)+'.mat'), {'prediction':prediction.astype(np.float32)})

def compute_prediction_parallel(ncores = 1,
                                plaintexts_path = './plaintexts',
                                predictions_path = './predictions',
                                save_mat = False):
    pool = Pool(ncores)
    doublets = list(range(8))
    pool.map(partial(compute_prediction_one_doublet, plaintexts_path = plaintexts_path, predictions_path = predictions_path, save_mat=save_mat), doublets)
    log.info("Predictions computed in {0} s.".format(time.clock()-t0))

def compute_correlation(predictions_path = './predictions',
                        traces_path = './traces',
                        correlations_path = './correlations',
                        save_mat = False):
    traces = np.load(os.path.join(traces_path, 'processed_masked_traces.npy'))
    if save_mat:
        sio.savemat(os.path.join(traces_path, 'processed_masked_traces.mat'), {'traces':traces.astype(np.float32)})
    log.info("Loaded {0} traces of {1} samples".format(np.shape(traces)[0], np.shape(traces)[1]))
    if True:
        raise MemoryError('Cannot compute correlation with numpy, use the MATLAB script')
    for doublet in range(1, nb_doublets):
        prediction = np.load(os.path.join(predictions_path, 'prediction_doublet_'+str(doublet)+'.npy'))
        log.info("Computing correlation for doublet {0}".format(doublet))
        correlation = corr.corr(traces, prediction)
        np.save(os.path.join(correlations_path, 'correlation_doublet_'+str(doublet)+'.npy'), correlation)
        if save_mat:
            sio.savemat(os.path.join(correlations_path, 'correlation_doublet_'+str(doublet)+'.mat'), {'correlation':correlation.astype(np.float32)})

def display_results(correct_key,
                    correlations_path = './correlations',
                    correlation_format = 'mat'):
    # Split key in doublets for display
    correct_key = [correct_key[i:i+4] for i in range(0, len(correct_key), 4)]
    for doublet, correct_doublet in enumerate(correct_key):
        if correlation_format == 'npy':
            corr = np.load('correlations/correlation_doublet_'+str(doublet)+'.npy')
        elif correlation_format == 'mat':
            corr = sio.loadmat('correlations/correlation_doublet_'+str(doublet)+'.mat')['correlation']
        max_correlation_per_key_doublet = abs(corr).max(axis=0)
        max_correlation_indexes = abs(corr).max(axis=1)
        hex_key_doublet = hex(np.argmax(max_correlation_per_key_doublet))[2:-1].zfill(4)
        index_of_interest = np.argmax(max_correlation_indexes)
        print "###\nKey doublet #{0} : \"{1}\", found at index {2}".format(str(doublet).zfill(1), hex_key_doublet.zfill(4), index_of_interest)
        position_correct_doublet = list(np.sort(max_correlation_per_key_doublet)[::-1]).index(max_correlation_per_key_doublet[int(correct_doublet, 16)])
        print "  Correct one is \"{0}\", ranked {1}/{2}".format(correct_doublet, position_correct_doublet, nb_k_hyp)

def plot_results(correct_key,
                 correlations_path = './correlations',
                 plots_path = './plots',
                 correlation_format = 'mat',
                 save = False):
    correct_key = [correct_key[i:i+4] for i in range(0, len(correct_key), 4)]
    for doublet, correct_doublet in enumerate(correct_key):
        if correlation_format == 'npy':
            corr = np.load('correlations/correlation_doublet_'+str(doublet)+'.npy')
        elif correlation_format == 'mat':
            corr = sio.loadmat('correlations/correlation_doublet_'+str(doublet)+'.mat')['correlation']
        max_correlation_per_key_doublet = abs(corr).max(axis=0)
        max_correlation_indexes = abs(corr).max(axis=1)
        hex_key_doublet = hex(np.argmax(max_correlation_per_key_doublet))[2:-1].zfill(4)
        index_of_interest = np.argmax(max_correlation_indexes)
        for i in range(np.shape(corr)[0]):
            plt.title('Correlation values for all key hypotheses')
            plt.xlabel('Key hypotheses')
            plt.ylabel('Correlation')
            if i == index_of_interest:
                plt.plot(corr[i,:], color='red', alpha=0.5)
            else:
                plt.plot(corr[i,:], color='#000000', alpha=0.25)
        if save:
            plt.savefig(os.path.join(plots_path, 'k_hyp', 'corr_VS_k_hyp_doublet_'+str(doublet)+'.png'))
        else:
            plt.show()
        plt.close()
        for i in range(np.shape(corr)[1]):
            plt.title('Correlation values for all sample combinations')
            plt.xlabel('Sample combinations')
            plt.ylabel('Correlation')
            if i == int(hex_key_doublet, 16):
                plt.plot(corr[:,i], color='red', alpha=0.5)
            else:
                plt.plot(corr[:,i], color='#000000', alpha=0.25)
        if save:
            plt.savefig(os.path.join(plots_path, 'combinations', 'corr_VS_combinations_doublet_'+str(doublet)+'.png'))
        else:
            plt.show()
        plt.close()
            
if __name__ == "__main__":
    t0 = time.time()
    compute_prediction(save_mat = True)
    # compute_prediction_parallel(ncores = 2, save_mat = True)
    print time.time() - t0
    compute_correlation(save_mat = True)
    # display_results(correct_key = '0123456789abcdef123456789abcdef0')
    # plot_results(correct_key = '0123456789ab', save = True)
    # plot_results(correct_key = '0123456789abcdef123456789abcdef0', save = True)
