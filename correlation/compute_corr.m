path = '../2nd_order/'
load(strcat(path, 'traces/processed_masked_traces.mat'));

head = {'doublet' 'correlation' 'k_hyp' 'sample'};
fid = fopen('corr_results.csv', 'w');
fprintf(fid, '%s,', head{1,1:end-1});
fprintf(fid, '%s\n', head{1,end});
fclose(fid);

for doublet=0:7
    doublet
    load(sprintf(strcat(path, 'predictions/prediction_doublet_%s.mat'), int2str(doublet)));
    correlation = corr(traces, prediction);
    correlation = abs(correlation);
    save(sprintf(strcat(path, 'correlations/correlation_doublet_%s.mat'), int2str(doublet)), 'correlation');
    max_k = max(correlation);
    max_s = max(correlation');
    [val_corr_at_k, index_k] = max(max_k)
    [val_corr_at_s, index_s] = max(max_s)
    dlmwrite('corr_results.csv', [doublet val_corr_at_k index_k-1 index_s], '-append');
end
exit