# Inputs :
#  - T of size n x t
#  - H of size n x h
# Output :
#  - matrix of size t x h

import numpy as np
np.seterr(all='raise')

import time

def corr(A,B):
    # Columnwise mean of input arrays & subtract from input arrays themeselves
    A_mA = A - A.mean(0)[None,:]
    B_mB = B - B.mean(0)[None,:]

    # Sum of squares across columns
    ssA = (A_mA**2).sum(0);
    ssB = (B_mB**2).sum(0);

    # Finally get corr coeff
    prod_mean = np.dot(A_mA.T,B_mB)
    sqr = np.sqrt(np.dot(ssA[:,None],ssB[None]))
    return np.dot(A_mA.T,B_mB)/np.sqrt(np.dot(ssA[:,None],ssB[None]))

def fast_corr(T,H,step=0):
    n, t = np.shape(T)
    n, h = np.shape(H)
    if not step:
        T_centered = T - np.mean(T, axis=0)
        H_centered = H - np.mean(H, axis=0)
        ACS = np.matmul(T_centered.T, H_centered)
        CS_t = np.sum(np.square(T_centered), axis=0)
        CS_h = np.sum(np.square(H_centered), axis=0)
        C = ACS/np.sqrt(np.outer(CS_t, CS_h))
        return C
    else:
        C = np.zeros((n//step, t, h))
        for i in range(n//step):
            T_updated = T[:i*step,:]
            H_updated = H[:i*step,:]
            T_current = T[i*step:(i+1)*step,:]
            H_current = H[i*step:(i+1)*step,:]

            T_current_centered = T_current - np.mean(T_current, axis=0)
            H_current_centered = H_current - np.mean(H_current, axis=0)

            ACS_current  = np.matmul(T_current_centered.T, H_current_centered)
            CS_t_current = np.sum(np.square(T_current_centered), axis=0)
            CS_h_current = np.sum(np.square(H_current_centered), axis=0)

            if i == 0:
                ACS_updated  = ACS_current
                CS_t_updated = CS_t_current
                CS_h_updated = CS_h_current
                T_updated    = T_current
                H_updated    = H_current
            else:
                delta_t = np.mean(T_updated, axis=0) - np.mean(T_current, axis=0)
                delta_h = np.mean(H_updated, axis=0) - np.mean(H_current, axis=0)
                ACS_updated  += ACS_current  + step*i*np.outer(delta_t, delta_h)/(i+1)
                CS_t_updated += CS_t_current + step*i*np.square(delta_t)/(i+1)
                CS_h_updated += CS_h_current + step*i*np.square(delta_h)/(i+1)

            try:
                C[i,:,:] = ACS_updated/np.sqrt(np.outer(CS_t_updated, CS_h_updated))
            except:
                raise ValueError("Zero denominator found when computing the correlation, consider increasing the step")

        return C
